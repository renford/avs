var keystone = require('keystone');
Appointment = keystone.list('Appointment');
Testimonial = keystone.list('Testimonial');
Slide = keystone.list('Slide');
ZipCode = keystone.list('ZipCode');
Project = keystone.list('Project');
WorkCategory = keystone.list('WorkCategory');
var braintree = require('braintree');
var cors = require('cors');
var middleware = require('./middleware');
var moment = require('moment');
var importRoutes = keystone.importer(__dirname);
const csvFilePath='./routes/Zip_Codes.csv';
const csv=require('csvtojson');
const EmailService = require('./../service/EmailService');
EmailService.init(keystone);
const Config = require('./../service/Config');
var mongoose = require('mongoose');

let zipCodeData = {};
csv()
.fromFile(csvFilePath)
.on('json',(jsonObj)=>{
	zipCodeData[jsonObj.ZIP] = parseCoordinates(jsonObj.the_geom)
})
.on('done',(error)=>{
	if (error){
		console.error(error);
	}
	console.log('end')
})

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

let environment =  braintree.Environment.Sandbox;
let merchantId = process.env.BRAINTREE_MERCHANT_ID_SANDBOX;
let publicKey = process.env.BRAINTREE_PUBLIC_KEY_SANDBOX;
let privateKey = process.env.BRAINTREE_PRIVATE_KEY_SANDBOX;

if (keystone.get('env') === 'production') {
	environment =  braintree.Environment.Production;
	merchantId = process.env.BRAINTREE_MERCHANT_ID;
	publicKey = process.env.BRAINTREE_PUBLIC_KEY;
	privateKey = process.env.BRAINTREE_PRIVATE_KEY;
} 

var gateway = braintree.connect({
	environment: environment, 
	merchantId: merchantId,
	publicKey: publicKey,
	privateKey: privateKey
  });

function parseCoordinates(val){
	val = val.substring(val.indexOf('(((') + 3, val.indexOf(')))'));
	let vals = val.split(',');
	let coords = vals.map((val)=> {
		val = val.trim();
		let cords = val.split(' ');
		return {lng: parseFloat(cords[0]), lat: parseFloat(cords[1])};
	});
	return coords;
}

// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
};

// Setup Route Bindings
exports = module.exports = function (app) {
	console.log('Keystone running with env : ' + keystone.get('env'));
	
	if (keystone.get('env') === 'development'){
		app.use(cors());
	}

	//app.get('/', routes.views.index);

	/*
	app.get('/', (req, res) => {
		res.redirect('/home');
	})
	This is just for example
	app.get('/', (req, res) => {
		res.send('index.html')
	})
	
	//app.get('/blog/:category?', routes.views.blog);
	//app.get('/blog/post/:post', routes.views.post);
	//app.get('/gallery', routes.views.gallery);
	//app.all('/contact', routes.views.contact);

	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);
	
	app.use(function (req, res) {
		res.render('index');
	});	
	*/

	app.get('/api/config', (req, res)=> {
		console.log('received request for /api/config..');
		WorkCategory.model.find()
		.exec(function(err, categories) {
			res.send({data:{workCategories: categories}});
		});
	});

	app.post('/api/checkZipCode', (req, res)=> {
		console.log('received request for /api/checkZipCode..');
		let zipCode = parseInt(req.body.zipCode);
		console.log('looking for zip code..' + zipCode);
		ZipCode.model.find({name: zipCode})
		.exec(function(err, zipCodes) {
			console.log('found zip codes ' + JSON.stringify(zipCodes));
			if (zipCodes.length) {
				res.send({service: true, data : zipCodeData[zipCode] })
			}
			else {
				res.send({service: false, data : zipCodeData[zipCode]});
			}
		});
	});

	app.get('/api/appointments', (req, res)=> {
		console.log('received request for /api/appointments..');
		Appointment.model.find({date: {'$gte': new Date()} })
		.sort('-startTime')
		.limit(100)
		.exec(function(err, appointments) {
			console.log('responsding with ' + appointments.length + ' appointments.');
			res.send({data:appointments});
		});
	});

	app.get('/api/testimonials', (req, res)=> {
		console.log('received request for /api/testimonials..');
		Testimonial.model.find({})
		.exec(function(err, testimonials) {
			console.log('responsding with ' + testimonials.length + ' testimonials.');
			res.send({data:testimonials});
		});
	});

	app.get('/api/projects', (req, res)=> {
		console.log('received request for /api/projects..');
		Project.model.find({})
		.exec(function(err, projects) {
			console.log('responsding with ' + projects.length + ' projects.');
			res.send({data:projects});
		});
	});

	app.get('/api/slides', (req, res)=> {
		console.log('received request for /api/slides..');
		Slide.model.find({})
		.exec(function(err, slides) {
			console.log('responding with ' + slides.length + ' slides.');
			res.send({data:slides});
		});
	});

	app.get('/api/workCategories', (req, res)=> {
		console.log('received request for /api/workCategories..');
		WorkCategory.model.find({})
		.exec(function(err, workCategories) {
			console.log('responding with ' + workCategories.length + ' workCategories.');
			res.send({data:workCategories});
		});
	});

	app.post('/api/checkout', (req, res) => {
		console.log('/api/checkout called for ' + JSON.stringify(req.body));
		let data = req.body.user;
		Appointment.model.find().exec((err, results) => {
			if (err){
				console.error(err);
				return res.status(500).send(err);
			}
			invoiceId = Config.invoiceStart + results.length;

			WorkCategory.model.find({name : { "$in" : data.workCategories }}).exec().then((cats) => {
				apt = new Appointment.model({date: data.appointmentDate, slot: data.appointmentSlot, 
					time: moment().hour(9).minute(0).add(data.appointmentSlot, 'hours').format('h:mm a'),
					'name.first': data.firstName, 'name.last': data.lastName , 
					addressLine1: data.addressLine1, 
					amountPaid: req.body.amount,
					addressLine2: data.addressLine2, zipCode: data.zipCode, phoneNumber: data.phoneNumber, email: data.email, 
					message: data.message, workCategories: cats, invoiceId: invoiceId});
				
				apt.save((err, appointment) => {
					if (err){
						console.error(err);
						return res.status(500).send(err);
					}
					
					gateway.transaction.sale({
						amount: req.body.amount,
						paymentMethodNonce: req.body.paymentMethodNonce,
						options: {
						submitForSettlement: true
						}
					}, (err, result) => {
						if (err){
							console.error(err);
							appointment.notes = 'PAYMENT FAILED :' + JSON.stringify(err);
							appointment.save();
							return res.status(500).send(err);	  
						}
						console.log('payment successful for ' + JSON.stringify(req.body) + ' with response ' + JSON.stringify(result));
						appointment.paymentId = result.transaction.id;
						appointment.save((err, result) => {
							if (err){
								console.error(err);
							}
							res.send(result);
						});
						EmailService.sendAppointmentConfirmationMail(appointment);
					});
				});
			})
			.catch((err)=> {
				console.log(err);
			});		
		});
	});

	app.get("/api/client_token", function (req, res) {
		gateway.clientToken.generate({}, function (err, response) {
		  res.send(response.clientToken);
		});
	});

	app.get("/login", function (req, res) {
		res.redirect('/keystone');
	});

	app.get("/admin", function (req, res) {
		res.redirect('/keystone');
	});

	app.get("/employee", function (req, res) {
		res.redirect('/');
	});

	app.get('/api/calendar', middleware.requireUser, function(req, res){
	//app.get('/api/calendar', function(req, res){
		console.log('/api/calendar called for ' + JSON.stringify(req.body));
		var dt = new Date();
		dt.setYear(dt.getYear()-1);
		var user = req.user;
		var query = {date: {'$gte': dt}, 'assignedTo': user._id};
		if (user.isAdmin){
			query = {date: {'$gte': dt}};
		}
		Appointment.model.find(query)
		.exec(function(err, appointments) {
			console.log('responsding with ' + appointments.length + ' appointments.');
			res.send({data:appointments});
		});
	});

	app.get('/api/user', middleware.requireUser, function(req, res){
	//app.get('/api/user', function(req, res){
		console.log('/api/user called for ' + JSON.stringify(req.body));
		res.send({data:req.user});
	});

	app.post('/api/balancecheckout', middleware.requireUser, (req, res) => {
	//app.post('/api/balancecheckout',  (req, res) => {
		console.log('/api/balancecheckout called for ' + JSON.stringify(req.body));
		Appointment.model.findOne({_id: mongoose.Types.ObjectId( req.body.appointment.id)}).exec((err, appointment) => {
			if (err){
				console.error(err);
				return res.status(500).send(err);
			}
					
			gateway.transaction.sale({
				amount: req.body.amount,
				paymentMethodNonce: req.body.paymentMethodNonce,
				options: {
					submitForSettlement: true
				}
			}, (err, result) => {
				if (err){
					console.error(err);
					appointment.notes = 'PAYMENT FAILED :' + JSON.stringify(err);
					appointment.save();
					return res.status(500).send(err);	  
				}
				console.log('payment successful for ' + JSON.stringify(req.body) + ' with response ' + JSON.stringify(result));
				appointment.balancePaymentId = result.transaction.id;
				appointment.balanceAmount = req.body.appointment.amount;
				appointment.fulfilled = true;
				appointment.customerSignatureData = req.body.appointment.customerSignatureData; 
				appointment.technicianSignatureData = req.body.appointment.technicianSignatureData;
				
				appointment.save((err, result) => {
					if (err){
						console.error(err);
					}
					res.send(result);
				});
				//EmailService.sendAppointmentConfirmationMail(appointment);
			})
			.catch((err)=> {
				console.log(err);
			});		
		});
	});

	app.get('/api/appointment/:id', middleware.requireUser, (req, res)=> {
		console.log('received request for /api/appointments..');
		Appointment.model.findOne({_id: mongoose.Types.ObjectId( req.params.id)}).exec((err, appointment) => {
			if (err){
				console.error(err);
				return res.status(500).send(err);
			}
			res.send({data:appointment});
		});
	});

	/*
	app.get("/*", function (req, res) {
		res.redirect('/');
	});
	*/
};



