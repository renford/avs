var nodemailer = require('nodemailer');
var Config = require('./Config.js');
var EmailTemplate = require('email-templates').EmailTemplate;
var path = require('path');
var moment = require('moment');
var keystone = null; 
var User = null;
var Appointment = null;
const ical = require('ical-generator');
var appointmentTemplate = new EmailTemplate(path.join(__dirname, 'templates', 'confirmationEmail'));
var calendarInviteTemplate = new EmailTemplate(path.join(__dirname, 'templates', 'calendarInvite'));

var transporter = nodemailer.createTransport({
	host: Config.mail.smtp.host,
    port: Config.mail.smtp.port,
    secure: Config.mail.smtp.secure,
    auth: {
        type: Config.mail.smtp.auth.type,
        clientId: Config.mail.smtp.auth.clientId,
        clientSecret: Config.mail.smtp.auth.clientSecret,
		user: Config.mail.smtp.auth.user, 
		refreshToken: Config.mail.smtp.auth.refreshToken, 
		accessToken: Config.mail.smtp.auth.accessToken
    }
});

function sendCalenderMail(appointment, user, method) {
	console.log('Sending calendar invite for appointment ' + JSON.stringify(appointment));
	Appointment.model.findOne({_id: appointment.id}).populate('workCategories').exec((err, appointment) => {
		if (err) {
			console.log(err);
		}
		User.model.findOne({_id : user}).exec((err, recepient) => {
			if (err){
				console.log(err);
			}
			else {
				console.log('Sending calendar invite to technician ' + JSON.stringify(recepient) + ' for appointment ' + JSON.stringify(appointment));
				var startDate = appointment.date;
				startDate.setHours(9 + appointment.slot);
				var endDate = appointment.date;
				endDate.setHours(9 + appointment.slot + 1);
				var cal = ical({domain: Config.domain, name: 'Mr Repairman LLC Appointment'});
				calendarInviteTemplate.render({appointment: appointment, 
					recepient: recepient, 
					workCategories: appointment.workCategories.map((category)=> {
						return category.name;
					})}, function(err, result){
					cal.createEvent({
						start: startDate,
						end: endDate,
						summary: Config.mail.appointment.description + ' at ' + appointment.addressLine1 + ', ' + appointment.addressLine2 ,
						description: result.html,
						location: appointment.addressLine1 + ', ' + appointment.addressLine2,
						url: 'http://' + Config.domain
					});
					transporter.sendMail({
						from: Config.mail.template.from + ' <' + Config.mail.smtp.auth.user + '>',
						to: recepient.email,
						subject: Config.mail.appointment.description,
						html: result.html,
						text: Config.mail.appointment.description + ' at ' + appointment.addressLine1 + ', ' + appointment.addressLine2,
						icalEvent: {
							method: 'request',
							content: cal.toString()
						}
						}, function(err, responseStatus) {
						if (err) {
							console.log(err);
							console.log('Error occured sending calendar invite for appointment ' + JSON.stringify(appointment) + ' to technician ' + JSON.stringify(recepient));
						} 
						else {
							console.log('Successfully send calendar invite for appointment ' + JSON.stringify(appointment));
						}
					});		
				});
			}
		});
	});
}

module.exports = {
	init: (service) => {
		keystone = service;
		User = keystone.list('User');
		Appointment = keystone.list('Appointment');
	},

	sendAppointmentConfirmationMail: function(appointment){
		console.log('Sending confirmation email for appointment ' + JSON.stringify(appointment));
		appointmentTemplate.render({appointment: appointment, date:moment(appointment.date).format('dddd[,] MMMM Do'), time : moment().hour(9).minute(0).add(appointment.slot, 'hours').format('h:mm a')}, function(err, result){
			if (err){
				console.log(err);
			}
			User.model.find({isAdmin: true}).exec((err, users) => {
				var mailOptions = {
					from: Config.mail.template.from,
					to: appointment.email,
					bcc: users.map((user) => {
						return user.email
					}),
					subject: 'Mr Repairman LLC Appointment Confirmation', 
					html: result.html
				};
				transporter.sendMail(mailOptions, function(err, res){
					if (err){
						console.log(err);
					}
				});	
			});
		});
	},

	cancelAppointment: function(appointment, user) {
		if (!user){
			return;
		}
		sendCalenderMail(appointment, user, 'CANCEL');
	},

	sendAppointment: function(appointment, user) {
		if (!user){
			return;
		}
		sendCalenderMail(appointment, user, 'request');
	},

	sendResetPasswordMail: function(user, cbk){
	
	}
};
