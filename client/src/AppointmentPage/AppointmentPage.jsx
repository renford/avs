import React from 'react';
import axios from 'axios';
import { systemConstants } from '../_constants';
import moment from 'moment';
import Dialog from 'material-ui/Dialog';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar';
import SelectField from 'material-ui/SelectField';
import async from 'async';
var dropin = require('braintree-web-drop-in');

import {
  Step,
  Stepper,
  StepLabel,
  StepContent,
  StepButton
} from 'material-ui/stepper'

import {
  RadioButton,
  RadioButtonGroup
} from 'material-ui/RadioButton'

import Checkbox from 'material-ui/Checkbox';

import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import SignatureCanvas from 'react-signature-canvas'
import './style.scss';

const HOST = systemConstants.SERVICE_URL ;

export class AppointmentPage extends React.Component {
 	constructor(props) {
	    super(props);
	    this.state = {
        saved: false,
        loading: true,
        paid: false,
        amount: 0,
        enablePayment: false
      };
      this.getClientToken();
	}

  componentWillMount() {
    axios.get(HOST + 'api/user').then(res => {
        if (res.data === 401){
          window.location.href = '/login';
        }
        this.setState({user: res.data.data, loading: false});
    });
  }

  componentDidMount() {
    this.initPaymentDropIn();
  }

  componentWillReceiveProps (nextProps) {
    this.custSigCanvas.clear();
    this.techSigCanvas.clear();
    if (nextProps.event) {
      axios.get(HOST + 'api/appointment/' + nextProps.event.data._id)
      .then( res => {
        this.props.event.data = res.data.data;
        if (this.props.event.data.customerSignatureData) {
          this.custSigCanvas.fromDataURL(this.props.event.data.customerSignatureData);
        }
        if (this.props.event.data.technicianSignatureData) {
          this.techSigCanvas.fromDataURL(this.props.event.data.technicianSignatureData);
        }
      })
      .catch( err => {
        console.error(err);
      });
    }
  }

  handleChangeStartTime = (event, date) => {
    date.setSeconds(0);
    this.state.startTime = date;
    this.setState({startTime: date});
    this.calculateAmout();
  }

  handleChangeEndTime = (event, date) => {
    date.setSeconds(0);
    this.state.endTime = date;
    this.setState({endTime: date});
    this.calculateAmout();
  }

  calculateAmout = () => {
    if ((this.state.startTime) && (this.state.endTime)){
        let hrs = Math.ceil((this.state.endTime.getTime() - this.state.startTime.getTime())/(1000*60*60));
        if (hrs > 1) {
          this.setState({amount: (hrs -1)*100});
        }
        else {
          this.setState({amount: 0});
        }
    }
  }

  onSave = () => {
    this.initPaymentDropIn();
    //this.setState({saved: true});
  }

  initPaymentDropIn = () => {
    dropin.create({
      authorization: this.state.clientToken,
      container: '#dropin-container'
    }, (createErr, instance) => {
          this.brainTree = instance;
    });
  }

  getClientToken = () => {
    axios.get(HOST + 'api/client_token').then(res => {
      this.state.clientToken = res.data;
      this.setState({clientToken: res.data});
      this.initPaymentDropIn();
    });
  }

  handlePay = () => {
    console.log(JSON.stringify(this.custSigCanvas.getTrimmedCanvas().toDataURL('image/png')));
    this.brainTree.requestPaymentMethod((requestPaymentMethodErr, payload) => {
      // Submit payload.nonce to your server
      axios.post(HOST + 'api/balancecheckout', {amount: this.state.amount, paymentMethodNonce: payload.nonce,
          appointment: {
            id: this.props.event.data._id,
            customerSignatureData: this.custSigCanvas.toDataURL('image/png'),
            technicianSignatureData: this.techSigCanvas.toDataURL('image/png')
          }
       })
      .then(response => {
        this.brainTree.teardown((teardownErr) => {
          if (teardownErr) {
            console.error('Could not tear down Drop-in UI!');
          } else {
            console.info('Drop-in UI has been torn down!');
            this.setState({paid: true});
          }
        });

        this.setState({ confirmationSnackbarMessage: "Payment succesfull", confirmationSnackbarOpen: true, processed: true })
      })
      .catch(err => {
        console.log(err)
        return this.setState({ confirmationSnackbarMessage: "Payment failed", confirmationSnackbarOpen: true })
      })
    });
  }

  checkCompleteness = () => {
    if ((this.state.startTime) && (this.state.endTime) && (this.state.amount)){
        this.setState({enablePayment: true});
    }
  }

  clearCustSignature = () => {
    this.custSigCanvas.clear();
  }

  clearTechSignature = () => {
    this.techSigCanvas.clear();
  }

  handleCustSignatureRef = (ref) => {
    this.custSigCanvas = ref;
    console.log('handleCustSignatureRef...');
    console.log(JSON.stringify(this.props.event));
    if (!this.props.event || !this.props.event.data.customerSignatureData) {
      console.log('clearing cust sign');
       this.custSigCanvas.clear();
    }
  }

  handleTechSignatureRef = (ref) => {
    this.techSigCanvas = ref;
      if (!this.props.event || !this.props.event.data.technicianSignatureData) {
       this.techSigCanvas.clear();
    }
  }

  createNotesMarkup = () => {
    return {__html: this.props.event ? this.props.event.data.invoiceNotes: ''};
  }

  render() {
    let payment = (<div></div>);
    if (this.state.paid) {
      payment = (<div className='conf'>Thank you for your payment of <span className='bold'>${this.state.amount}</span>.
                </div>);
    }
    else {
      payment = (<div className='col-md-4'>
      <div id="dropin-container"></div>
      <RaisedButton
        style={{ display: 'block' }}
        label="Pay"
        labelStyle={{ fontSize: 22}}
        primary={true}
        disabled={this.state.paid}
        onClick={() => this.handlePay()}
        style={{ marginBottom: 5, marginTop: 20, maxWidth: 100, height: 40}} />
      </div>);
    }

    return (
      <div className='appointment'>
        <div className='row'>

         <Card className='col-md-4'>
          <CardHeader
            title="Customer Information"
          />
          <CardText>
            <div className='row'>
              <div className='col-md-4'>
                 Name
              </div>
              <div className='col-md-8'>
                  {this.props.event ? this.props.event.data.name.first: ''} {this.props.event ? this.props.event.data.name.last: ''}
              </div>
            </div>
            <div className='row'>
              <div className='col-md-4'>
                 Address
              </div>
              <div className='col-md-8'>
                  {this.props.event ? this.props.event.data.addressLine1: ''}, {this.props.event ? this.props.event.data.addressLine2: ''}, {this.props.event ? this.props.event.data.zipCode: ''}
              </div>
            </div>
            <div className='row'>
              <div className='col-md-4'>
                 Contact #
              </div>
              <div className='col-md-8'>
                  {this.props.event ? this.props.event.data.phoneNumber: ''}
              </div>
            </div>

            <div className='row'>
              <div className='col-md-4'>
                 Email
              </div>
              <div className='col-md-8'>
                  {this.props.event ? this.props.event.data.email: ''}
              </div>
            </div>

            <div className='row'>
              <div className='col-md-4'>
                 Appointment #
              </div>
              <div className='col-md-8'>
                  {this.props.event ? this.props.event.data.invoiceId: ''}
              </div>
            </div>

            <div className='row'>
              <div className='col-md-4'>
                 Appointment Time
              </div>
              <div className='col-md-8'>
                  {this.props.event ? this.props.event.start.toLocaleString(): ''}
              </div>
            </div>
          </CardText>
        </Card>

        <Card className='col-md-4'>
          <CardHeader
            title="Technician Information"
          />
          <CardText>
            <div className='row'>
              <div className='col-md-4'>
                 Name
              </div>
              <div className='col-md-8'>
                  {this.state.user ? this.state.user.name.first: ''} {this.state.user ? this.state.user.name.last: ''}
              </div>
            </div>
            <div className='row'>
              <div className='col-md-4'>
                 Phone Number
              </div>
              <div className='col-md-8'>
                  {this.state.user ? this.state.user.phoneNumber: ''}
              </div>
            </div>

            <div className='row'>
              <div className='col-md-4'>
                Notes
              </div>
              <div className='col-md-8' dangerouslySetInnerHTML={this.createNotesMarkup()}>
              </div>
            </div>
 
          </CardText>
        </Card>

        <Card className='col-md-4'>
          <CardHeader
            title="Project Information"
          />
          <CardText>
            <div className='row'>
              <div className='col-md-4'>
                 Start Time
              </div>
                <TimePicker
                  format="ampm"
                  hintText="Start Time"
                  value={this.state.startTime}
                  onChange={this.handleChangeStartTime}
                />
            </div>
            <div className='row'>
              <div className='col-md-4'>
                 End Time
              </div>
              <TimePicker
                  format="ampm"
                  hintText="Start Time"
                  value={this.state.endTime}
                  onChange={this.handleChangeEndTime}
                />
            </div>
             <div className='row'>
              <div className='col-md-4'>
                 Amount Balance
              </div>
              <div className='col-md-8'>
                  {this.state.amount} USD
              </div>
            </div>

            <div className='row'>
              <div className='col-md-4'>
                 Completed
              </div>
              <div className='col-md-8'>
                  {this.props.event ? this.props.event.data.fulfilled ? 'YES': 'NO': ''} 
              </div>
            </div>

            <div className='row'>
              <div className='col-md-4'>
                 Total Paid
              </div>
              <div className='col-md-8'>
                  {this.props.event ? this.props.event.data.amountPaid + (this.props.event.data.balancePaid ? this.props.event.data.balancePaid : 0) : 0} USD
              </div>
            </div>
         </CardText>
        </Card>
      </div>

      <div className='row'>
        <Card className='col-md-4'>
          <CardHeader
            title="Customer Signature"
          />
          <CardText>
              <SignatureCanvas ref={this.handleCustSignatureRef} penColor='blue'
                canvasProps={{width: window.innerWidth/3 - 30, height: 100, className: 'sigCanvas'}} />
                <FlatButton label="Clear" onClick={this.clearCustSignature} style={{margin: 12}} />
          </CardText>
        </Card>

        <Card className='col-md-4'>
          <CardHeader
            title="Technician Signature"
          />
          <CardText>
              <SignatureCanvas ref={this.handleTechSignatureRef} penColor='black'
                canvasProps={{width: window.innerWidth/3 - 30, height: 100, className: 'sigCanvas'}} />
               <FlatButton label="Clear" onClick={this.clearTechSignature} style={{margin: 12}} />
          </CardText>
        </Card>
      {payment}
    </div>

      <Snackbar
        open={this.state.confirmationSnackbarOpen || this.state.loading}
        message={this.state.loading ? 'Loading... ' :this.state.confirmationSnackbarMessage || ''}
        autoHideDuration={10000}
        onRequestClose={() => this.setState({ confirmationSnackbarOpen: false })} />
    </div>
    );
	}
}
