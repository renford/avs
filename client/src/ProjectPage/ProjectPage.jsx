import React from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import './style.scss';
import {Image, Video, Transformation, CloudinaryContext} from 'cloudinary-react';

export class ProjectPage extends React.Component {
 	constructor(props) {
	    super(props);
	    this.state = {
        slides: null,
        rendered: 0,
        slideSettings : {
          dots: true,
          infinite: true,
          pauseOnHover: true,
          slidesToShow: 1,
          centerMode: true,
          autoplay: false,
          slidesToScroll: 1,
          variableWidth: true,
          adaptiveHeight: true
        }
      };
	    this.setSlides();
	}

    setSlides = () => {
      let items = [];
      if (this.props.project.images.length > 0) {
        console.log('creating good slides');
        this.props.project.images.forEach((item) => {
          items.push(this.getSlide(item));
        });
      }
      this.state.slides = items;
    }

    getSlide = (image) => {
      return (
        <div className='carousel-img'>
            <Image cloudName="djm3ipovr" version={image.version} format={image.format} publicId={image.public_id}>
                <Transformation height="600" crop="fill"/>
            </Image>
        </div>
      );
    }

    render() {
      let slides = (<div className='slide'></div>);
      if (this.state.slides){
        slides = this.state.slides;
      }
      else {
        console.log('sssss');
      }
      return (
        <div className='project-dialog'>
          <Slider {...this.state.slideSettings}>
              {slides}
          </Slider>
        </div>
      );
	}
}


