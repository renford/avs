import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
//import { userActions } from '../_actions';
import style from './style.scss';
import Dialog from 'material-ui/Dialog';
import Card from 'material-ui/Card';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Snackbar from 'material-ui/Snackbar';
import async from 'async';
import axios from 'axios';
import moment from 'moment';
import { systemConstants } from '../_constants';
import {Map} from './../_components/Map';
var FontAwesome = require('react-fontawesome');
var dropin = require('braintree-web-drop-in');
import 'bootstrap/scss/bootstrap-grid.scss';

import {
  Step,
  Stepper,
  StepLabel,
  StepContent,
  StepButton
} from 'material-ui/stepper'

import {
  RadioButton,
  RadioButtonGroup
} from 'material-ui/RadioButton'

import Checkbox from 'material-ui/Checkbox';

import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
//import logo from './../../dist/assets/logo.svg';

// should be '/' in PROD
const HOST = systemConstants.SERVICE_URL ;

const styles  = {
  subLabelActive: {
    fontSize: 16,
    fontFamily: 'Montserrat',
    color: 'rgb(10,10,10)'
  },

  subLabel: {
    fontSize: 16,
    fontFamily: 'Montserrat',
    color: 'rgb(150,150,150)'
  },

  checkbox : {
    marginTop: 10
  }
};

export class SchedulePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            amount: 50,
            loading: true,
            paid : false,
            submitted: false,
            navOpen: false,
            confirmationModalOpen: false,
            confirmationTextVisible: false,
            stepIndex: 0,
            appointmentDateSelected: false,
            tomorrow: this.getNextBusinessDay(),
            appointmentMeridiem: 0,
            validFirstName: false,
            validLastName: false,
            validEmail: false,
            validPhone: false,
            validAddressLine1: false,
            validAddressLine2: false,
            smallScreen: window.innerWidth < 768,
            confirmationSnackbarOpen: false,
            workCategories: [],
            config : {
              workCategories: []
            },
            completedSteps: {},
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNavToggle = this.handleNavToggle.bind(this)
        this.handleNextStep = this.handleNextStep.bind(this)
        this.handleSetAppointmentDate = this.handleSetAppointmentDate.bind(this)
        this.handleSetAppointmentSlot = this.handleSetAppointmentSlot.bind(this)
        this.handleSetAppointmentMeridiem = this.handleSetAppointmentMeridiem.bind(this)
        this.validateEmail = this.validateEmail.bind(this)
        this.validatePhone = this.validatePhone.bind(this)
        this.checkDisableDate = this.checkDisableDate.bind(this)
        this.renderAppointmentTimes = this.renderAppointmentTimes.bind(this)
        this.renderConfirmationString = this.renderConfirmationString.bind(this)
        this.renderAppointmentConfirmation = this.renderAppointmentConfirmation.bind(this)
        this.resize = this.resize.bind(this);
        this.getClientToken();
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }

    getNextBusinessDay = () => {
      var day = new Date();
      let delta = 1 ;
      if (day.getDay() === 5) {
        delta = 3 ;
      }
      else if (day.getDay() === 6){
        delta = 2 ;
      }
      day.setDate(day.getDate() + delta);
      return day;
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({ submitted: true });
        const { user } = this.state;
        const { dispatch } = this.props;
        if (user.email) {
            dispatch(userActions.register(user));
        }
    }

    /* scheduler code */
    handleNavToggle() {
      return this.setState({ navOpen: !this.state.navOpen })
    }

    handleNextStep() {
      const { stepIndex } = this.state
      this.state.completedSteps[stepIndex] = true;
      return (stepIndex < 7) ? this.setState({ stepIndex: stepIndex + 1, completedSteps: this.state.completedSteps }) : null
    }

    handleSetAppointmentDate(date) {
      this.handleNextStep();
      this.setState({ appointmentDate: date, confirmationTextVisible: true })
    }

    handleSetAppointmentSlot(slot) {
      this.handleNextStep()
      this.setState({ appointmentSlot: slot })
    }

    handleSetLocationType = (type) => {
      this.handleNextStep()
      this.setState({ locationType: type })
    }

    handleSetAppointmentMeridiem(meridiem) {
      this.setState({ appointmentMeridiem: meridiem})
    }

    handleFetch(response) {
      const { config, appointments } = response
      const initSchedule = {}
      const today = moment().startOf('day')
      initSchedule[today.format('YYYY-DD-MM')] = true
      const schedule = !appointments.length ? initSchedule : appointments.reduce((currentSchedule, appointment) => {
        const { date, slot } = appointment
        const dateString = moment(date, 'YYYY-DD-MM').format('YYYY-DD-MM')
        !currentSchedule[date] ? currentSchedule[dateString] = Array(8).fill(false) : null
        Array.isArray(currentSchedule[dateString]) ?
          currentSchedule[dateString][slot] = true : null
        return currentSchedule
      }, initSchedule)
      for (let day in schedule) {
        let slots = schedule[day]
        slots.length ? (slots.every(slot => slot === true)) ? schedule[day] = true : null : null
      }
      this.setState({
        schedule,
        config : {workCategories: config.workCategories },
        loading: false
      })
    }

    handleFetchError(err) {
      console.log('Error fetching data:' + err)
      this.setState({ confirmationSnackbarMessage: 'Error fetching data', confirmationSnackbarOpen: true })
    }

    handleSubmit() {
      const appointment = {
        date: moment(this.state.appointmentDate).format('YYYY-DD-MM'),
        slot: this.state.appointmentSlot,
        name: this.state.firstName + ' ' + this.state.lastName,
        email: this.state.email,
        phone: this.state.phone
      }
      axios.post(HOST + 'api/appointments', )
      axios.post(HOST + 'api/appointments', appointment)
      .then(response => this.setState({ confirmationSnackbarMessage: "Appointment succesfully added!", confirmationSnackbarOpen: true, processed: true }))
      .catch(err => {
        console.log(err)
        return this.setState({ confirmationSnackbarMessage: "Appointment failed to save.", confirmationSnackbarOpen: true })
      })
    }

    validateEmail(email) {
      const regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
      return regex.test(email) ? this.setState({ email: email, validEmail: true }) : this.setState({ validEmail: false })
    }

    validatePhone(phoneNumber) {
      const regex = /^(1\s|1|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/
      return regex.test(phoneNumber) ? this.setState({ phoneNumber: phoneNumber, validPhone: true }) : this.setState({ validPhone: false })
    }

    validateFirstName = (name) => {
      if ((name.length > 1)) {
        this.setState({ firstName: name, validFirstName: true });
      }
      else {
        this.setState({ validFirstName: false });
      }
    }

    validateLastName = (name) => {
      if ((name.length > 2)) {
        this.setState({ lastName: name, validLastName: true });
      }
      else {
        this.setState({ validLastName: false });
      }
    }

    validateAddressLine1 = (addressLine) => {
      if ((addressLine.length > 5) && (addressLine.indexOf(' ') > -1 )) {
        this.setState({ addressLine1: addressLine, validAddressLine1: true });
      }
      else {
        this.setState({ validAddressLine1: false })
      }
    }

    validateAddressLine2 = (addressLine) => {
      if (addressLine.length > 0) {
        this.setState({ addressLine2: addressLine, validAddressLine2: true });
      }
      else {
        this.setState({ validAddressLine2: false })
      }
    }

    checkDisableDate(day) {
      const dateString = moment(day).format('YYYY-DD-MM')
      return this.state.schedule[dateString] === true || moment(day).startOf('day').diff(moment().startOf('day')) < 0 || day.getDay() === 0 || day.getDay() === 6
    }

    renderConfirmationString() {
      const spanStyle = {color: '#00bcd4'}
      return this.state.confirmationTextVisible ? <h3 style={{ textAlign: this.state.smallScreen ? 'center' : 'left', color: '#bdbdbd', lineHeight: 1.5, padding: '0 10px', fontFamily: 'Montserrat'}}>
        { <span>
          Scheduling appointment {this.state.appointmentDate && <span>
            on <span style={spanStyle}>{moment(this.state.appointmentDate).format('dddd[,] MMMM Do')}</span>
        </span>} {Number.isInteger(this.state.appointmentSlot) && <span>at <span style={spanStyle}>{moment().hour(9).minute(0).add(this.state.appointmentSlot, 'hours').format('h:mm a')}</span></span>}
        </span>}
      </h3> : null
    }

    isSlotTaken = (appointmentDateString, slot) => {
      if (!this.state.schedule[appointmentDateString] ){
        return false;
      }
      let slots = this.state.schedule[moment(this.state.appointmentDate).format('YYYY-DD-MM')];
      if ((slots[slot]) || (slots[slot + 1])){
        return true;
      }
      return false;
    }

    renderAppointmentTimes() {
      if (!this.state.loading) {
        const slots = [...Array(8).keys()]
        return slots.map(slot => {
          const appointmentDateString = moment(this.state.appointmentDate).format('YYYY-DD-MM')
          const t1 = moment().hour(9).minute(0).add(slot, 'hours')
          const t2 = moment().hour(9).minute(0).add(slot + 1, 'hours')
          const scheduleDisabled = this.isSlotTaken(appointmentDateString, slot);
          const meridiemDisabled = this.state.appointmentMeridiem ? t1.format('a') === 'am' : t1.format('a') === 'pm'
          return <RadioButton
            label={t1.format('h:mm a')}
            key={slot}
            value={slot}
            labelStyle= {{fontFamily: 'Montserrat', fontSize: 16,fontWeight:700, color: 'rgb(150,150,150)'}}
            style={{marginBottom: 15, display: meridiemDisabled ? 'none' : 'inherit'}}
            disabled={scheduleDisabled || meridiemDisabled}/>
        })
      } else {
        return (<div></div>);
      }
    }

    addWorkCategory = (event, flag) => {
      let name = event.target.offsetParent.textContent;
      if (flag && this.state.workCategories.indexOf(name) === -1){
        this.state.workCategories.push(name);
        this.setState({workCategories: this.state.workCategories});
      }
      else if (!flag && this.state.workCategories.indexOf(name) > -1){
        this.state.workCategories.splice(this.state.workCategories.indexOf(name), 1);
        this.setState({workCategories: this.state.workCategories});
      }
   }

   isWorkCategoryAdded = (name) => {
      return (this.state.workCategories.indexOf(name) > -1);
    }

    renderWorkCategories = () => {
     if (!this.state.loading) {
      let checkboxes = (this.state.config.workCategories.map(category => {
        return <Checkbox
          label={category.name}
          checked={this.isWorkCategoryAdded(category.name)}
          onCheck={this.addWorkCategory}
          style={styles.checkbox}
          labelStyle={this.isWorkCategoryAdded(category.name) ? styles.subLabelActive: styles.subLabel }
           style={{marginBottom: 15}}
        />
      }));
      return <div className='checkboxes'>{checkboxes}</div>
    }
      return (<div></div>);
    }

    renderAppointmentConfirmation() {
      const spanStyle = { color: '#00bcd4' }
      return <section>
        <p>Name: <span style={spanStyle}>{this.state.firstName} {this.state.lastName}</span></p>
        <p>Number: <span style={spanStyle}>{this.state.phone}</span></p>
        <p>Email: <span style={spanStyle}>{this.state.email}</span></p>
        <p>Appointment: <span style={spanStyle}>{moment(this.state.appointmentDate).format('dddd[,] MMMM Do[,] YYYY')}</span> at <span style={spanStyle}>{moment().hour(9).minute(0).add(this.state.appointmentSlot, 'hours').format('h:mm a')}</span></p>
      </section>
    }
    resize() {
      this.setState({ smallScreen: window.innerWidth < 768 })
    }

    componentWillMount() {
      async.series({
        config(callback) {
          axios.get(HOST + 'api/config').then(res => {
            callback(null, res.data.data)
          })
        },
        appointments(callback) {
          axios.get(HOST + 'api/appointments').then(res => {
            callback(null, res.data.data)
          })
        }
      }, (err,response) => {
        err ? this.handleFetchError(err) : this.handleFetch(response)
      })
      addEventListener('resize', this.resize)
    }

    componentWillUnmount() {
      removeEventListener('resize', this.resize)
    }

    handleZipCodeChange = (zipCode) => {
      this.setState({zipCodeData: null, zipCode: zipCode});
      if (zipCode.length > 5) {
        zipCode = zipCode.substr(0,5);
        this.setState({zipCode: zipCode});
      }
      if (zipCode.length === 5) {
        axios.post(HOST + 'api/checkZipCode', {zipCode: zipCode})
        .then(response => {
          this.setState({zipCodeValid: true, zipCodeData: response.data.data, outsideServiceArea: !response.data.service});
        });
      }
      else {
        this.setState({zipCodeValid: false});
      }
    }

    handleInstructionsChange = (instructions) => {
      this.setState({customerInstructions: instructions});
    }

    updateCheck() {
      this.setState((oldState) => {
        return {
          checked: !oldState.checked,
        };
      });
    }

    getClientToken = () => {
      axios.get(HOST + 'api/client_token').then(res => {
        this.setState({clientToken: res.data});
      });
    }

    initPaymentDropIn = () => {
      dropin.create({
        authorization: this.state.clientToken,
        container: '#dropin-container'
      }, (createErr, instance) => {
            this.brainTree = instance;
      });
    }

    handlePay = () => {
      this.brainTree.requestPaymentMethod((requestPaymentMethodErr, payload) => {
        // Submit payload.nonce to your server
        axios.post(HOST + 'api/checkout', {amount: this.state.amount, paymentMethodNonce: payload.nonce,
          user : {
            email: this.state.email, firstName: this.state.firstName, lastName: this.state.lastName,
            phoneNumber: this.state.phoneNumber, zipCode : this.state.zipCode, locationType: this.state.locationType,
            workCategories: this.state.workCategories, appointmentDate: this.state.appointmentDate, appointmentSlot: this.state.appointmentSlot,
            addressLine1: this.state.addressLine1, addressLine2 : this.state.addressLine2
          }})
        .then(response => {
          this.brainTree.teardown((teardownErr) => {
            if (teardownErr) {
              console.error('Could not tear down Drop-in UI!');
            } else {
              console.info('Drop-in UI has been torn down!');
              // Remove the 'Submit payment' button
              this.setState({paid: true, appointment: response.data});
            }
          });

          this.setState({ confirmationSnackbarMessage: "Appointment succesfully added!", confirmationSnackbarOpen: true, processed: true })
        })
        .catch(err => {
          console.log(err)
          return this.setState({ confirmationSnackbarMessage: "Appointment failed to save.", confirmationSnackbarOpen: true })
        })
      });
    }

    isAddressFormValid = () => {
      return this.state.validFirstName && this.state.validLastName && this.state.validEmail && this.state.validPhone && this.state.validAddressLine1 ;
    }

    render() {
        const { registering  } = this.props;
        const { user, submitted, paid, completedSteps, outsideServiceArea, zipCode, zipCodeData, zipCodeValid, stepIndex, loading, navOpen, smallScreen, confirmationModalOpen, confirmationSnackbarOpen, ...data } = this.state;
        const contactFormFilled = data.firstName && data.lastName && data.phone && data.email && data.validPhone && data.validEmail && data.validAddressLine1 && data.validAddressLine2;

        const modalActions = [
          <FlatButton
            label="Cancel"
            primary={false}
            onClick={() => this.setState({ confirmationModalOpen : false})} />,
          <FlatButton
            label="Confirm"
            primary={true}
            onClick={() => this.handleSubmit()} />
        ];
        let zipResult = (<div></div>);
        let map = (<div></div>);
        let tickIndicator = (<div></div>);

        if (zipCodeData) {
          map  = <Map center={zipCodeData[0]} paths={zipCodeData}></Map>
        }

        if (outsideServiceArea){
          tickIndicator = (<div className='tickOutside'>
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 20 20" fill="red">
              <path d="M17.815 1.112c-0.214-0.174-0.529-0.142-0.703 0.073l-1.741 2.143c-0.985-1.971-3.022-3.328-5.371-3.328-3.308 0-6 2.692-6 6 0 1.536 0.298 3.22 0.884 5.008 0.367 1.118 0.848 2.279 1.433 3.462l-2.206 2.715c-0.174 0.214-0.142 0.529 0.073 0.703 0.093 0.075 0.204 0.112 0.315 0.112 0.145 0 0.29-0.063 0.388-0.185l1.93-2.375c1.377 2.562 2.732 4.296 2.789 4.369 0.095 0.121 0.24 0.191 0.393 0.191s0.298-0.070 0.393-0.191c0.057-0.073 1.418-1.814 2.797-4.385 0.812-1.513 1.46-2.999 1.925-4.416 0.587-1.787 0.884-3.472 0.884-5.008 0-0.552-0.075-1.087-0.216-1.595l2.104-2.589c0.174-0.214 0.142-0.529-0.073-0.703zM5 6c0-2.757 2.243-5 5-5 2.122 0 3.939 1.329 4.664 3.198l-1.675 2.062c0.007-0.086 0.011-0.172 0.011-0.26 0-1.654-1.346-3-3-3s-3 1.346-3 3 1.346 3 3 3c0.301 0 0.591-0.045 0.866-0.128l-3.851 4.74c-1.031-2.173-2.014-4.945-2.014-7.613zM10 8c-1.103 0-2-0.897-2-2s0.897-2 2-2c1.103 0 2 0.897 2 2s-0.897 2-2 2zM15 6c0 3.248-1.458 6.652-2.682 8.935-0.903 1.686-1.816 3.027-2.318 3.726-0.5-0.696-1.407-2.029-2.309-3.71-0.062-0.115-0.124-0.234-0.187-0.355l7.462-9.184c0.023 0.193 0.035 0.389 0.035 0.587z" ></path>
            </svg>
            <div className='outsideservicearea'>Sorry, this is outside our service area</div>
          </div>);
        }
        else {
          tickIndicator = (<div className='tick'>
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 20 20" fill="green">
              <path d="M10 20c-0.153 0-0.298-0.070-0.393-0.191-0.057-0.073-1.418-1.814-2.797-4.385-0.812-1.513-1.46-2.999-1.925-4.416-0.587-1.787-0.884-3.472-0.884-5.008 0-3.308 2.692-6 6-6 1.244 0 2.437 0.377 3.451 1.091 0.226 0.159 0.28 0.471 0.121 0.697s-0.471 0.28-0.697 0.121c-0.844-0.594-1.838-0.909-2.875-0.909-2.757 0-5 2.243-5 5 0 3.254 1.463 6.664 2.691 8.951 0.902 1.681 1.809 3.014 2.309 3.71 0.483-0.672 1.346-1.938 2.214-3.533 1.192-2.19 2.642-5.468 2.776-8.649 0.012-0.276 0.245-0.49 0.521-0.479s0.49 0.245 0.478 0.521c-0.063 1.498-0.399 3.132-0.998 4.855-0.475 1.368-1.117 2.796-1.908 4.246-1.343 2.464-2.636 4.118-2.69 4.187-0.095 0.121-0.24 0.191-0.393 0.191z"></path>
              <path d="M10 9c-0.128 0-0.256-0.049-0.354-0.146l-2.5-2.5c-0.195-0.195-0.195-0.512 0-0.707s0.512-0.195 0.707 0l2.146 2.146 5.146-5.146c0.195-0.195 0.512-0.195 0.707 0s0.195 0.512 0 0.707l-5.5 5.5c-0.098 0.098-0.226 0.146-0.354 0.146z" ></path>
            </svg>
          </div>);
        }

        if (zipCodeValid){
          zipResult =(
          <div className='horizontal'>
            {map}
            {tickIndicator}
          </div>);
        }

        if (stepIndex == 7) {
          this.initPaymentDropIn();
        }
        let payment = (<div></div>);
        if (this.state.paid) {
          payment = (<div className='conf'>Thank you for your payment of <span className='bold'>${this.state.appointment.amountPaid}</span>.
          Your appointment has been confirmed for <span className='bold'>{moment(this.state.appointment.date).format('dddd[,] MMMM Do')}</span> at
          <span className='bold'>{moment().hour(9).minute(0).add(this.state.appointment.slot, 'hours').format('h:mm a')}</span>. You confirmation number is <span className='bold'>{this.state.appointment.invoiceId}</span> and
          a confirmation email has been sent to your mail <span className='bold'>{this.state.appointment.email}</span>. We look forward to see you then!
          </div>);
        }
        else {
          payment = (<div>
          <div id="dropin-container"></div>
          <RaisedButton
            style={{ display: 'block' }}
            label="Pay"
            labelStyle={{ fontSize: 22}}
            primary={true}
            disabled={paid}
            onClick={() => this.handlePay()}
            style={{ marginBottom: 5, marginTop: 20, maxWidth: 100, height: 40}} />
          </div>);
        }
        return (
          <div className="appointment">
            <section style={{
              width: '100%',
              margin: 'auto',
              marginTop: 0,
            }}>
              <Card style={{
                  fontSize: '2rem',
                  fontFamily: 'Montserrat',
                  padding: '10px 10px 25px 10px',
                  height: smallScreen ? '100vh' : null
                }}>
                <Stepper
                  activeStep={stepIndex}
                  linear={true}
                  orientation="vertical">
                  <Step disabled={loading}>
                    <StepButton onClick={() => this.setState({ stepIndex: 0 })}>
                      <span className={stepIndex === 0 ? 'labelStyleActive': 'labelStyle' }>What is the location of your project?</span>
                    </StepButton>
                    <StepContent  style={{ paddingLeft: 70}}>
                    <TextField
                          value={zipCode}
                          style={{ display: 'block' }}
                          inputStyle={{ paddingTop: 30, fontWeight:'bolder'}}
                          name="zipCode"
                          maxLength={5}
                          minLength={5}
                          floatingLabelText="Zip Code"
                          onChange={(evt, newValue) => this.handleZipCodeChange(newValue)}/>
                    {zipResult}
                    <div>
                      <RaisedButton
                        style={{ display: 'block' }}
                        label="Next"
                        labelStyle={{ fontSize: 22}}
                        primary={true}
                        onClick={() => this.handleNextStep()}
                        disabled={!zipCodeValid }
                        style={{ marginBottom: 5, marginTop: 20, maxWidth: 100, height: 40}} />
                      </div>
                      </StepContent>
                  </Step>

                  <Step disabled={loading || Object.keys(completedSteps).length < 1}>
                    <StepButton onClick={() => this.setState({ stepIndex: 1 })}>
                    <span className={stepIndex === 1 ? 'labelStyleActive': 'labelStyle' }>What kind of location is this?</span>
                    </StepButton>
                    <StepContent>
                      <RadioButtonGroup
                          style={{ marginTop: 15,
                                  paddingLeft: 30,
                                  marginLeft: 15
                                }}
                          name="locationType"
                          defaultSelected={data.locationType}
                          onChange={(evt, val) => this.handleSetLocationType(val)}>
                          <RadioButton
                            label="Residence"
                            value="residence"
                            labelStyle={this.state.locationType === 'residence' ? styles.subLabelActive : styles.subLabel}
                            style={{marginBottom: 15}}/>

                          <RadioButton
                            label="Commercial"
                            value="commercial"
                            labelStyle={this.state.locationType === 'commercial' ? styles.subLabelActive : styles.subLabel}
                            style={{marginBottom: 15}}/>
                        </RadioButtonGroup>
                      </StepContent>
                  </Step>

                  <Step disabled={loading || Object.keys(completedSteps).length < 2}>
                    <StepButton onClick={() => this.setState({ stepIndex: 2 })}>
                    <span className={stepIndex === 2 ? 'labelStyleActive': 'labelStyle' }>What services do you need?</span>
                    </StepButton>
                    <StepContent style={{paddingLeft: 60}}>
                    {this.renderWorkCategories()}
                    <RaisedButton
                        style={{ display: 'block' }}
                        label="Next"
                        labelStyle={{ fontSize: 22}}
                        primary={true}
                        onClick={() => this.handleNextStep()}
                        disabled={!this.state.workCategories.length}
                        style={{ marginBottom: 5, marginTop: 20, maxWidth: 100, height: 40}} />
                    </StepContent>
                  </Step>

                  <Step disabled={loading || Object.keys(completedSteps).length < 3}>
                    <StepButton onClick={() => this.setState({ stepIndex: 3 })}>
                    <span className={stepIndex === 3 ? 'labelStyleActive': 'labelStyle' }>Choose an available day for your appointment</span>
                    </StepButton>
                    <StepContent  style={{paddingLeft: 60}}>
                      <DatePicker
                          style={{
                            marginTop: 10,
                            marginLeft: 10
                          }}
                          hintText="Select a date"
                          autoOk={true}
                          formatDate={new Intl.DateTimeFormat('en-US', {
                            day: 'numeric',
                            weekday: 'long',
                            month: 'long',
                            year: 'numeric',
                          }).format}
                          value={data.appointmentDate}
                          mode={smallScreen ? 'portrait' : 'landscape'}
                          onChange={(n, date) => this.handleSetAppointmentDate(date)}
                          shouldDisableDate={day => this.checkDisableDate(day)}
                          />
                      </StepContent>
                  </Step>

                  <Step disabled={ !data.appointmentDate || Object.keys(completedSteps).length < 4}>
                    <StepButton onClick={() => this.setState({ stepIndex: 4 })}>
                    <span className={stepIndex === 4 ? 'labelStyleActive': 'labelStyle' }>Choose an available time for your appointment</span>
                    </StepButton>
                    <StepContent  style={{paddingLeft: 60}}>
                      <SelectField
                        floatingLabelText="AM or PM"
                        value={data.appointmentMeridiem}
                        onChange={(evt, key, payload) => this.handleSetAppointmentMeridiem(payload)}
                        selectionRenderer={value => value ? 'PM' : 'AM'}>
                        <MenuItem value={0}>AM</MenuItem>
                        <MenuItem value={1}>PM</MenuItem>
                      </SelectField>
                      <RadioButtonGroup
                        style={{ marginTop: 15,
                                marginLeft: 15
                              }}
                        name="appointmentTimes"
                        defaultSelected={data.appointmentSlot}
                        onChange={(evt, val) => this.handleSetAppointmentSlot(val)}>
                        {this.renderAppointmentTimes()}
                      </RadioButtonGroup>
                    </StepContent>
                  </Step>

                  <Step disabled={ !Number.isInteger(this.state.appointmentSlot) || Object.keys(completedSteps).length < 5}>
                    <StepButton onClick={() => this.setState({ stepIndex: 5 })}>
                    <span className={stepIndex === 5 ? 'labelStyleActive': 'labelStyle' }>Share your contact information with us and we'll send you a reminder</span>
                    </StepButton>
                    <StepContent style={{paddingLeft: 60}}>
                      <section>

                        <div className='row'>
                          <div className='col-sm'>
                            <TextField
                              style={{ display: 'block' }}
                              inputStyle={{ paddingTop: 30, fontWeight:'bolder', color: 'green'}}
                              name="first_name"
                              hintText="First Name"
                              floatingLabelText="First Name"
                              errorText={data.validFirstName ? null : 'Enter first name'}
                              onChange={(evt, newValue) => this.validateFirstName(newValue)}/>
                          </div>
                          <div className='col-sm'>
                            <TextField
                              style={{ display: 'block' }}
                              inputStyle={{ paddingTop: 30, fontWeight:'bolder', color: 'green'}}
                              name="last_name"
                              hintText="Last Name"
                              floatingLabelText="Last Name"
                              errorText={data.validLastName ? null : 'Enter last name'}
                                onChange={(evt, newValue) => this.validateLastName(newValue)}/>
                          </div>
                        </div>
                        <div className='row'>
                          <div className='col-sm'>
                            <TextField
                            style={{ display: 'block' }}
                            inputStyle={{ paddingTop: 30, fontWeight:'bolder', color: 'green'}}
                            name="email"
                            hintText="name@mail.com"
                            floatingLabelText="Email"
                            errorText={data.validEmail ? null : 'Enter a valid email address'}
                            onChange={(evt, newValue) => this.validateEmail(newValue)}/>
                          </div>
                          <div className='col-sm'>
                            <TextField
                              style={{ display: 'block' }}
                              inputStyle={{ paddingTop: 30, fontWeight:'bolder', color: 'green'}}
                              name="phone"
                              hintText="(888) 888-8888"
                              floatingLabelText="Phone"
                              errorText={data.validPhone ? null: 'Enter a valid phone number'}
                              onChange={(evt, newValue) => this.validatePhone(newValue)} />
                          </div>
                       </div>

                      <div className='row'>
                          <div className='col-sm'>
                            <TextField
                              style={{ display: 'block' }}
                              inputStyle={{ paddingTop: 30, fontWeight:'bolder', color: 'green'}}
                              name="streetName"
                              floatingLabelText="Street Address"
                              errorText={data.validAddressLine1 ? null: 'Enter street name'}
                              onChange={(evt, newValue) => this.validateAddressLine1(newValue)} />
                          </div>

                          <div className='col-sm'>
                            <TextField
                              style={{ display: 'block' }}
                              inputStyle={{ paddingTop: 30, fontWeight:'bolder', color: 'green'}}
                              name="houseApptNumber"
                              floatingLabelText="House/Appt/Unit #"
                              errorText={data.validAddressLine2 ? null: 'Enter house/appartment/unit #'}
                              onChange={(evt, newValue) => this.validateAddressLine2(newValue)} />
                          </div>
                      </div>
                        <RaisedButton
                          style={{ display: 'block' }}
                          label="Next"
                          labelStyle={{ fontSize: 22}}
                          primary={true}
                          onClick={() => this.handleNextStep()}
                          disabled={!this.isAddressFormValid() }
                          style={{ marginBottom: 5, marginTop: 20, maxWidth: 100, height: 40}} />

                      </section>
                    </StepContent>
                  </Step>

                  <Step disabled={ !data.appointmentDate || Object.keys(completedSteps).length < 6}>
                    <StepButton onClick={() => this.setState({ stepIndex: 6 })}>
                    <span className={stepIndex === 6 ? 'labelStyleActive': 'labelStyle' }>Please let us you if you have any specific instructions</span>
                    </StepButton>
                    <StepContent  style={{paddingLeft: 60}}>
                    <TextField
                              style={{ display: 'block' }}
                              inputStyle={{ paddingTop: 30, fontWeight:'bolder', color: 'blue'}}
                              name="instructions"
                              hintText="Type your instructions here"
                              floatingLabelText="Instructions"
                              onChange={(evt, newValue) => this.handleInstructionsChange(newValue)} />

                      <RaisedButton
                          style={{ display: 'block' }}
                          label="Next"
                          labelStyle={{ fontSize: 22}}
                          primary={true}
                          onClick={() => this.handleNextStep()}
                          style={{ marginBottom: 5, marginTop: 20, maxWidth: 100, height: 40}} />

                    </StepContent>

                  </Step>

                  <Step disabled={Object.keys(completedSteps).length < 7}>
                    <StepButton onClick={() => this.setState({ stepIndex: 7 })}>
                    <span className={stepIndex === 7 ? 'labelStyleActive': 'labelStyle' }>Make a payment</span>
                    </StepButton>
                    <StepContent style={{paddingLeft: 60, width: 450}}>
                      {payment}
                    </StepContent>
                  </Step>

                </Stepper>
              </Card>
              <Dialog
                modal={true}
                open={confirmationModalOpen}
                actions={modalActions}
                title="Confirm your appointment">
                {this.renderAppointmentConfirmation()}
              </Dialog>

              <Snackbar
                open={this.state.confirmationSnackbarOpen || this.state.loading}
                message={this.state.loading ? 'Loading... ' :this.state.confirmationSnackbarMessage || ''}
                autoHideDuration={10000}
                onRequestClose={() => this.setState({ confirmationSnackbarOpen: false })} />

            </section>

          </div>
        );
    }
}
