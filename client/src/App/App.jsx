import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom';

import { connect } from 'react-redux';
//import { history } from '../_helpers';
//import { alertActions } from '../_actions';
import { PrivateRoute } from '../_components';
import { HomePage } from '../HomePage';
//import { LoginPage } from '../LoginPage';
/*
import { RegisterPage } from '../RegisterPage';
import { VerifyPage } from '../VerifyPage';
*/
import {AppointmentPage} from '../AppointmentPage';
import { EmployeePage } from '../EmployeePage';

export class App extends React.Component {
    constructor(props) {
        super(props);
        /*
        const { dispatch } = this.props;
        history.listen((location, action) => {
            dispatch(alertActions.clear());
        });
        */
    }

    render() {
        const { alert } = this.props;
        return (
            <div className="">
                <Router>
                    <div>
                        <div>
                            {/*
                            <PrivateRoute exact path="/" component={HomePage} />
                            */}
                            <Route exact path="/" component={HomePage} />
                            {/*
                            <Route exact path="/login" component={LoginPage} />
                            */}
                            <Route path="/employee" component={EmployeePage} />
                        </div>
                    </div>
                </Router>
            </div>
        );
    }
}