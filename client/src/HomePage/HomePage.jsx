import React from 'react';
import { Link } from 'react-router-dom';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import axios from 'axios';
import mainLogo from'./logo.png';
import './bluebackground-new.jpg';
import './sample-drawing2.jpg';
import imgFacebook from './facebook.png';
import imgYelp from './yelp.png';
import imgHouzz from './houzz.jpg';
import {Image, Video, Transformation, CloudinaryContext} from 'cloudinary-react';
import imgLightNormal from './light-normal.png';
import { systemConstants } from '../_constants';
import { SchedulePage } from '../SchedulePage';
import { ProjectPage } from '../ProjectPage';
import '../lib/jquery/dist/jquery.min.js';
import '../lib/Slicebox/css/slicebox.css';
import '../lib/Slicebox/css/custom.css';
import './../lib/Slicebox/js/jquery.slicebox';
import ScrollAnim from 'rc-scroll-anim';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import Animate from 'rc-animate';
const ScrollLink = ScrollAnim.Link;
const Element = ScrollAnim.Element;
const ScrollOverPack = ScrollAnim.OverPack;
const EventListener = ScrollAnim.Event;
var Shuffle = require('react-shuffle');
import './style.scss';
const HOST = systemConstants.SERVICE_URL ;

export class HomePage extends React.Component {
  constructor(props) {
      super(props);

      this.state = {
        appointmentModalOpen: false,
        projectModalOpen: false,
        appointmentTitle: 'Schedule an Appointment for $50 and get first hour of service free',
        testimonials : [],
        projects: [],
        slides: [],
        workCategories: [],
        showIdx: 0,
        testimonialsSettings : {
          dots: false,
          infinite: true,
          pauseOnHover: true,
          speed: 1000,
          slidesToShow: 1,
          centerMode: false,
          autoplay: false,
          slidesToScroll: 1,
          autoplaySpeed: 4000
        },

      };

      this.customContentStyle = {
        width: '80%',
        height: '95%',
        maxWidth: 'none'
      };

      this.modalActions = [
        <RaisedButton
          label="Close"
          secondary={true}
          onClick={() => this.setState({ appointmentModalOpen : false})} />,
      ];

      this.projectModalActions = [
        <RaisedButton
          label="Close"
          secondary={true}
          onClick={() => this.setState({ projectModalOpen : false})} />,
      ];
  }

  componentDidMount() {
    this.chat();
    EventListener.addEventListener('resize.userResize', this.barAnimate.bind(this));
  }

  onFocus = (e) => {
    this.dom = e.target;
    console.log(this.dom);
    this.barAnimate();
  }

  barAnimate = () => {
    if (!this.dom) {
      return;
    }
    const bar = this.bar;
    bar.style.left = `${this.dom.getBoundingClientRect().left}px`;
  }

  componentWillMount() {
    axios.get(HOST + 'api/testimonials').then(res => {
      this.setState({testimonials: this.getTestimonialSlides(res.data.data)});
    });
    axios.get(HOST + 'api/projects').then(res => {
      this.setState({projects: this.getProjectSections(res.data.data)});
    });
    axios.get(HOST + 'api/slides').then(res => {
      this.setState({slides: this.getSlides(res.data.data)});
    });
    axios.get(HOST + 'api/workCategories').then(res => {
      let cats = this.getWorkCategories(res.data.data);
      this.setState({workCategories: cats});
    });
  }

  setAppointmentTitle = (title) => {
    this.setState({appointmentTitle: 'Schdule an Appointment: ' + title});
  }

  getTestimonialSlides = (testimonials) => {
    let items = [];
    if (testimonials.length > 0) {
        testimonials.forEach((testimonial) => {
        items.push(this.getTestimonialSlide(testimonial));
      });
    }
    return items;
  }

  getTestimonialSlide = (testimonial) => {
    return (
      <div className="pixfort_pix_1">
        <div className="section">
          <a href={testimonial.linkSource} target='_blank'>
            <img src={imgLightNormal} alt="" className="q-icon"></img>
            <div className="testimonial">
                <p className="quote-text editContent">
                  {testimonial.comment}
                </p>
                <div className="quote-dots">. . .</div>
                <div className="quote-title editContent pix_text">
                  {testimonial.commentor}
                </div>
                <div className="quote-link editContent pix_text">
                  via {testimonial.linkLabel}
                </div>
            </div>
            </a>
        </div>
      </div>
    );
  }

  getProjectSections = (projects) => {
    let sections = [];
    if (projects.length > 0) {
        projects.forEach((project, idx) => {
          sections.push(this.getProjectSection(project));
      });
    }
    return sections;
  }

  getSlides = (slides) => {
    let items = [];
    if (slides.length > 0) {
      slides.forEach((item, idx) => {
        items.push(this.getSlide(item));
      });
    }
    return items;
  }

  getSlide = (item) => {
    return (
      <li>
          <Image cloudName="djm3ipovr" version={item.image.version} format={item.image.format} publicId={item.image.public_id}>
          </Image>
      </li>
    );
  }

  getWorkCategories = (workCategories) => {
    let items = [];
    if (workCategories.length > 0) {
        workCategories.forEach((item, idx) => {
        items.push(this.getWorkCategory(item, idx));
      });
    }
    return items;
  }

  getWorkCategory = (item, idx) => {
    return (
      <div key={idx} className="col-md-3 workcat">
        <div className='work-cat-inner'>
          <Image cloudName="djm3ipovr" version={item.image ? item.image.version : null} format={item.image ? item.image.format: null} publicId={item.image ? item.image.public_id : null}>
            <Transformation width="200" height="200" crop="fill"/>
          </Image>
          <div className='workcat-content'>
            <div className='header'>
              {item.name}
            </div>
            <div className="workcat-details">
              {item.description}
            </div>
          </div>
        </div>
      </div>
    );
  }

  getProjectSection = (project) => {
    return (
      <div className="project">
        <div className="project-inner" onClick={() => {this.closeChat(); this.openProjectPage(project)}}>
          <Image cloudName="djm3ipovr" version={project.mainImage.version} format={project.mainImage.format} publicId={project.mainImage.public_id}>
            <Transformation height="300" width="300" crop="fill"/>
          </Image>
          <div className="project-content">
            <div className='header'>
            </div>
            <div className='project-details'>
              <div className="">{project.name}</div>
              <div className="">{project.description}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  chat = () => {
    var t = window.driftt = window.drift = window.driftt || [];
    if (!t.init) {
      if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
      t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ],
      t.factory = function(e) {
        return function() {
          var n = Array.prototype.slice.call(arguments);
          return n.unshift(e), t.push(n), t;
        };
      }, t.methods.forEach(function(e) {
        t[e] = t.factory(e);
      }), t.load = function(t) {
        var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
        o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
        var i = document.getElementsByTagName("script")[0];
        i.parentNode.insertBefore(o, i);
      };
    }
    drift.SNIPPET_VERSION = '0.3.1';
    drift.load('ikuxxyuifdar');
  }

  openProjectPage = (project) => {
    this.setState({selectedProject: project, projectModalOpen: true});
  };

  openProjectClose = () => {
    this.setState({projectModalOpen: false});
  }

  openChat = () => {
    console.log('open chat...');
    if (window.drift && window.drift.api){
      window.drift.api.sidebar.open();
    }
  }

  closeChat = () => {
    console.log('close chat...');
    if (window.drift && window.drift.api){
      window.drift.api.sidebar.close();
    }
  }

  openScheduler = () => {
    this.closeChat();
    this.setState({appointmentModalOpen: true});
  }

  sliceboxReady = () => {
   let slicebox = $( '#sb-slider' );
   if (slicebox && !this.slicebox) {
      this.slicebox = slicebox;
      this.slicebox = this.slicebox.slicebox( {
        onReady : () => {
          //this.slicebox.play();
        },
        orientation : 'r',
        autoplay : true,
        colorHiddenSides : 'rgba(0,0,0,0)',
        disperseFactor : 0,
        cuboidsRandom : true,
        perspective : 1200,
        interval: 4000,
      });

    }
  }

  componentDidUpdate() {
    this.sliceboxReady();
  }

  render() {
      var slides = (<div></div>);
      if (this.state.slides.length) {
        slides = (<ul id="sb-slider" className="sb-slider">
        {this.state.slides}
      </ul>);
      }

      var testimonials = (<div></div>);
      if (this.state.testimonials.length) {
        testimonials =  (<Slider {...this.state.testimonialsSettings}>
          {this.state.testimonials}
        </Slider>);
      }

      return (
        <div>
          <Dialog
            modal={true}
            open={this.state.appointmentModalOpen}
            actions={this.modalActions}
            title={this.state.appointmentTitle}
            autoScrollBodyContent={true}
            contentStyle={this.customContentStyle}>
            <SchedulePage></SchedulePage>
          </Dialog>

          <Dialog
            modal={false}
            open={this.state.projectModalOpen}
            actions={this.projectModalActions}
            title={this.state.selectedProject ? this.state.selectedProject.name: ''}
            autoScrollBodyContent={false}
            onRequestClose={this.openProjectClose}
            contentStyle={this.customContentStyle}>
            <ProjectPage project={this.state.selectedProject}></ProjectPage>
          </Dialog>

          <div className="nav">
            <div className="nav-wap">
              <ScrollLink
                className="nav-list logo hoverlink"
                to="page0"
                showHeightActive="300"
                onFocus={this.onFocus}
              >
                <img src={mainLogo} className="logoImg" alt=""></img>
              </ScrollLink>

              <ScrollLink
                className="nav-list hoverlink hide-mobile"
                to="page1"
                showHeightActive="300"
                onFocus={this.onFocus}
              >
                SERVICES
              </ScrollLink>

              <ScrollLink
                className="nav-list hoverlink hide-mobile"
                to="page2"
                showHeightActive="300"
                onFocus={this.onFocus}
              >
                PROJECTS
              </ScrollLink>


              {/*
              <ScrollLink
                className="nav-list hoverlink hide-mobile"
                to="page3"
                showHeightActive="300"
                onFocus={this.onFocus}
              >
                TESTIMONIALS
              </ScrollLink>
              */}

              <ScrollLink
                className="nav-list hoverlink hide-mobile"
                to="pageqqqq"
                showHeightActive="300"
                onFocus={this.onFocus}
              >
                CAREERS
              </ScrollLink>

              <ScrollLink
                className="nav-list hoverlink hide-mobile"
                to="page3"
                showHeightActive="300"
                onFocus={this.onFocus}
              >
                TESTIMONIALS
              </ScrollLink>

              <div ref={(c) => { this.bar = c; }} className="nav-bar" />
            </div>
            <div className='right-nav-header'>
              <div className='right-link hoverlink hide-mobile' onClick={this.openChat}>CONTACT</div>
              <div onClick={this.openScheduler} className="scheduler pix_header_button pix_builder_bg" data-selector=".pix_builder_bg" rel="">Schedule an Appointment</div>
            </div>
          </div>

          <Element className="page0" id="page0">
            <video autoPlay muted loop className='background-video'>
              <source src="chicago.webm" type="video/mp4"></source>
            </video>
            <div className='video-shade'>
            </div>
            <div className='video-overlay'>
              <div className='slideshow'>
                {slides}
              </div>
              <div className='container headerDescription'>
                Mr Repairman is your source for professional, experienced and local home repairs of all types. Trusted by homeowners and business owners in Chicagoland since 2010
              </div>
            </div>
          </Element>

          <ScrollOverPack
            playScale={[0.2, 0.3]}
            id="page1"
            className="page1 page"
            key="0"
          >
            <QueueAnim key="2" className="workcategories-row row">
              {this.state.workCategories}
            </QueueAnim>

          </ScrollOverPack>

          <ScrollOverPack
            playScale={[0.3,0.3]}
            className="page page2"
            id="page2"
            key="3"
           >
            <QueueAnim key="5" className="project-row row">
              {this.state.projects}
            </QueueAnim>

          </ScrollOverPack>

          {/*
          <ScrollOverPack
            playScale={0.2}
            className="page3 page"
            id="page3"
            key="7"
          >
            <Animate key="9" transitionName="fade" transitionAppear>
              <div id="testimonial">
                {testimonials}
              </div>
            </Animate>
          </ScrollOverPack>
          */}

          <ScrollOverPack
            playScale={0.2}
            className="page4 page"
            id="pageqqqq"
            key="8">
            <div className="container">
              hdsdhj
            </div>

          </ScrollOverPack>

          <ScrollOverPack
            playScale={0.2}
            className="page3 page"
            id="page3"
            key="7"
          >
            <Animate key="9" transitionName="fade" transitionAppear>
              <div id="testimonial">
                {testimonials}
              </div>
            </Animate>
          </ScrollOverPack>


          <div className="pixfort_agency_14" id="section_agency_5">
            <div className="foot_st">
              <div className="container ">
                <div className="col-md-12 center_text">
                  <div className="f1_style"><span className="editContent"><span className="pix_text"><strong>Schedule an appointment for $50 and get first hour of service free.</strong></span></span></div>
                  <div className="f2_style"><span className="editContent"><span className="site_text">Flat service charge $100 per hour</span></span></div>
                  <div className="second_link">
                    <span className="get_2_btn "><a onClick={this.openScheduler} className="slow_fade" href="#">SCHEDULE AN APPOINTMENT</a></span>
                  </div>
                  <div className="rights_st"><span className="editContent"><span className="pix_text">All rights reserved Copyright 2018</span></span></div>
                  <span className="editContent">
                    <span className="pix_text">
                      <span className="rights_st"></span>   <span className="pixfort_st">Mr Repairman LLC</span>
                    </span>
                  </span>
                  <div className="clearfix"></div>
                  <div className="employeeLogin"><Link to="/employee">Employee Login</Link></div>

                  <div className="center_btn center_text pix_inline_block">
                    <a href="https://www.yelp.com/biz/mr-repairman-chicago" target='_blank' className="slow_fade social_button"><img src={imgYelp}></img></a>
                    {/*
                    <a href="#" className="slow_fade social_button"><img src={imgHouzz}></img></a>
                    <a href="#" className="slow_fade social_button"><img src={imgFacebook}></img></a>
                    */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
}
