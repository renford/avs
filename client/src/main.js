import React from 'react'
import ReactDOM from 'react-dom'
import './styles/main.scss'
import { Provider } from 'react-redux';
//import { store } from './_helpers';
import { App } from './App';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

// setup fake backend
//import { configureFakeBackend } from './_helpers';
//configureFakeBackend();

// Store Initialization
// ------------------------------------
//const store = createStore(window.__INITIAL_STATE__)

// Render Setup
// ------------------------------------
const MOUNT_NODE = document.getElementById('root')

let render = () => {
  //const App = require('./components/App').default
  //const routes = require('./routes/index').default(store)

  ReactDOM.render(
    <Provider>
       <MuiThemeProvider>
        <App />
      </MuiThemeProvider>
    </Provider>,
    MOUNT_NODE
  )
}

// Development Tools
// ------------------------------------
if (__DEV__) {
  if (module.hot) {
    const renderApp = render
    const renderError = (error) => {
      const RedBox = require('redbox-react').default

      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    }

    render = () => {
      try {
        renderApp()
      } catch (e) {
        console.error(e)
        renderError(e)
      }
    }

    // Setup hot module replacement
    module.hot.accept([
      './App',
      //'./routes/index',
    ], () =>
      setImmediate(() => {
        ReactDOM.unmountComponentAtNode(MOUNT_NODE)
        render()
      })
    )
  }
}

// Let's Go!
// ------------------------------------
if (!__TEST__) render()
