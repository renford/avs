var keystone = require('keystone');
var Types = keystone.Field.Types;
const EmailService = require('./../service/EmailService');
	
/**
 * Appointment Model
 * =============
 */

var previousAssignedTo = null;
var previousCancel = false;

var Appointment = new keystone.List('Appointment', {
	nocreate: true,
	noedit: false,
	nodelete: true,
	sortable :true,
	defaultColumns: ['name', 'date', 'invoiceId', 'workCategories', 'zipCode', 'assignedTo', 'addressLine1', 'phoneNumber', 'createdAt', 'fulfilled'],
	searchFields :['name', 'customerInstructions', 'assignedTo', 'fulfilled', 'date', 'zipCode', 'phoneNumber', 'workCategories', 'addressLine1', 'addressLine2']
});

Appointment.add({
	createdAt: { label: 'Date Invoice created', type: Date, default: Date.now, required: true, noedit: true},
	lastUpdate: { type: Date, default: Date.now, required: true, noedit: true },
	date: { label: 'Project Date', type: Types.Date, required: true, index: true},
	time: {type: String, required: true},
	slot: {type: Number, hidden: true},
	name: { label: 'Customer Name', type: Types.Name, required: true, index: true, noedit: true},
	addressLine1: { label: 'Customer Address', type: String, required: true, index: true, noedit: true},
	addressLine2: { type: String, required: false, noedit: true },
	zipCode: { type: Number, required: true , index: true, noedit: true},
	phoneNumber: { type: String, required: true, index: true, noedit: true},
	email: { type: Types.Email, required: true, index: true, noedit: true },
	message: { type: String, required: false, noedit: true },
	workCategories: { type: Types.Relationship, ref: 'WorkCategory', required: false, many: true,noedit: true},
	paymentId: {type: String, required: false, noedit: true},
	invoiceId: {type: String, required: true, index: true, noedit: true}, 
	invoiceNotes: {type: Types.Html, required: false, wysiwyg: true},
	customerInstructions: {type: Types.Html, required: false, wysiwyg: true, noedit: true},
	companyNotes: {type: Types.Html, wysiwyg: true, required: false},
	amountPaid: {type: Types.Money, required: true, noedit: true},
	balancePaid: {type: Types.Money, noedit: true},
	balancePaymentId: {type: String, noedit: true},
	assignedTo: { type: Types.Relationship, ref: 'User', required: false },
	fulfilled: {type: Types.Boolean, default: false}, 
	customerSignatureData: {type: String, noedit: true, hidden:true},
	customerSignature: {type: Types.Html, watch: true, noedit: true, wysiwyg: true, value: function() {
			return '<p><img src=\'' + this.customerSignatureData + '\'/></p>';
		}
	},
	technicianSignatureData: {type: String, noedit: true, hidden:true},	
	technicianSignature: {type: Types.Html, watch: true, noedit: true, wysiwyg: true, value: function () {
			return '<p><img src=\'' + this.technicianSignatureData + '\'/></p>';
		}
	},
	cancel: {type: Types.Boolean, default: false}
});

Appointment.schema.pre('save', function (next) {
	this.wasNew = this.isNew;
	previousAssignedTo = this.assignedTo;
	previousCancel = this.cancel;	
	next();
});

Appointment.schema.post('save', function () {
	if (!previousCancel && this.cancel) {
		if (previousAssignedTo || this.assignedTo) {
			if (previousAssignedTo !== this.assignedTo){
				EmailService.cancelAppointment(this, previousAssignedTo);
				EmailService.cancelAppointment(this, this.assignedTo);
			}
			else {
				EmailService.cancelAppointment(this, this.assignedTo);
			}
		}
		return;
	}

	else if (previousAssignedTo && (previousAssignedTo !== this.assignedTo)){
		EmailService.cancelAppointment(this, previousAssignedTo);
	}

	else if (!previousAssignedTo && this.assignedTo){
		EmailService.sendAppointment(this, this.assignedTo);		
	}

});

Appointment.defaultSort = '-invoiceId';

Appointment.register();
