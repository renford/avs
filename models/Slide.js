var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Slide Model
 * =============
 */

var Slide = new keystone.List('Slide', {
	autokey: { from: 'name', path: 'key', unique: true },
});

Slide.add({
	name: { type: String, required: true },
	image: { type: Types.CloudinaryImage }
});

Slide.register();
