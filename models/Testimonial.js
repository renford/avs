var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Testimonial Model
 * ==================
 */

var Testimonial = new keystone.List('Testimonial', {
	map: { name: 'comment' },
	autokey: { path: 'slug', from: 'comment', unique: true },
	sortable :true,
	defaultColumns: ['comment', 'sourceName', 'sourceLink'],
	searchFields : ['comment', 'sourceName', 'sourceLink']
});

Testimonial.add({
	comment: { type: String },
	commentor: { type: String },
	linkLabel: {type: String },
	linkSource: { type: String}
});

Testimonial.register();
