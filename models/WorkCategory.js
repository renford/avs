var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * WorkCategory Model
 * ==================
 */

var WorkCategory = new keystone.List('WorkCategory', {
	autokey: { from: 'name', path: 'key', unique: true },
});

WorkCategory.add({
	name: { type: String, required: true },
	description: { type: String},
	image: { type: Types.CloudinaryImage },
	workDetailTypes: { type: Types.Relationship, ref: 'WorkDetailType', many: true }
});


WorkCategory.relationship({ ref: 'User', path: 'users', refPath: 'workCategories' });
WorkCategory.relationship({ ref: 'Appointment', path: 'appointments', refPath: 'workCategories' });

WorkCategory.register();
