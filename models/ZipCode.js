var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * ZipCode Model
 * =============
 */

var ZipCode = new keystone.List('ZipCode', {
	autokey: { from: 'name', path: 'key', unique: true },
});

ZipCode.add({
	name: { type: Number, required: true },
	description: { type: String}
});

ZipCode.register();
